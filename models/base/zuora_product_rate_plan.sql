WITH source AS (

	SELECT *
	FROM {{var('schema')}}.productrateplan


), renamed AS(

	SELECT
	    id                          as product_rate_plan_id,
        name                        as product_rate_plan_name,

		--keys	
		product_id                  as product_id,

		-- info
        description                 as description,
        effective_start_date::DATE  as effective_start_date,
        effective_end_date::DATE    as effective_end_date,

		--metadata
		updated_by_id               as updated_by_id,
		updated_date                as updated_date,
		created_by_id               as created_by_id,
		created_date                as created_date

	FROM source

)

SELECT *
FROM renamed
