WITH source AS (

	SELECT *
	FROM {{var('schema')}}.product


), renamed AS(

	SELECT
	    id                          as product_id,
        name                        as product_name,

		-- info
        sku                         as sku,
        description                 as description,
        category                    as category,
        effective_end_date::DATE    as effective_end_date,
        effective_start_date::DATE  as effective_start_date,

		--metadata
		updated_by_id               as updated_by_id,
		updated_date                as updated_date,
		created_by_id               as created_by_id,
		created_date                as created_date

	FROM source

)

SELECT *
FROM renamed





