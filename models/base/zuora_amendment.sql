WITH source AS (

	SELECT *
	FROM {{var('schema')}}.amendment


), renamed AS(

	SELECT
	    id                       as amendment_id,

		--keys	
		subscription_id          as subscription_id,

		-- info
        name                     as amendment_name,
        type                     as amendment_type,

        status                   as status,
        code                     as code,

        current_term_period_type as current_term_period_type,
        renewal_term_period_type as renewal_term_period_type,
        renewal_setting          as renewal_setting,

        effective_date           as effective_date,
        contract_effective_date  as contract_effective_date,
        customer_acceptance_date as customer_acceptance_date,
        service_activation_date  as service_activation_date,
        suspend_date             as suspend_date,

		--metadata
		updated_by_id            as updated_by_id,
		updated_date             as updated_date,
		created_by_id            as created_by_id,
		created_date             as created_date

	FROM source

)

SELECT *
FROM renamed