WITH source AS (

	SELECT *
	FROM {{var('schema')}}.rateplan


), renamed AS(

	SELECT
	    id                     as rate_plan_id,
        name                   as rate_plan_name,

		--keys	
		subscription_id        as subscription_id,
		product_id             as product_id,
		product_rate_plan_id   as product_rate_plan_id,

		-- info
		amendment_id           as amendment_id,
		amendment_type         as amendment_type,

		--metadata
		updated_by_id          as updated_by_id,
		updated_date           as updated_date,
		created_by_id          as created_by_id,
		created_date           as created_date

	FROM source

)

SELECT *
FROM renamed