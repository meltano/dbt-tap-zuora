WITH source AS (

	SELECT *
	FROM {{var('schema')}}.contact

), renamed AS(

	SELECT 
		id               as contact_id,

		-- keys
		account_id       as account_id,

		-- contact info
		first_name       as first_name,
		last_name        as last_name,
		nick_name,
		address1         as street_address,
		address2         as street_address2,
		county,
		state,
		postal_code      as postal_code,
		city,
		country,
		tax_region       as tax_region,
		work_email       as work_email,
		work_phone       as work_phone,
		other_phone      as other_phone,
		other_phone_type as other_phone_type,
		fax,
		home_phone       as home_phone,
		mobile_phone     as mobile_phone,
		personal_email   as personal_email,
		description,


		-- metadata
		created_by_id    as created_by_id,
		created_date     as created_date,
		updated_by_id    as updated_by_id,
		updated_date     as updated_date

	FROM source

)

SELECT *
FROM renamed