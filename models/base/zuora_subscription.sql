WITH source AS (

	SELECT *
	FROM {{var('schema')}}.subscription


), renamed AS(

	SELECT
		id                                  		as subscription_id,
		name                                		as subscription_name,
	  	{{zuora_slugify("name")}}					as subscription_name_slugify,

		--keys
		account_id                           		as account_id,
		creator_account_id                    		as creator_account_id,
		creator_invoice_owner_id               		as creator_invoice_owner_id,
		invoice_owner_id                      		as invoice_owner_id,
		original_id                          		as original_id,
		previous_subscription_id              		as previous_subscription_id,
		cpq_bundle_json_id__qt                 		as cpq_bundle_json_id,
		
		-- info
		status                              		as subscription_status,
		auto_renew                           		as auto_renew,
		version                             		as version,
		term_type                            		as term_type,
		notes                               		as notes,
		is_invoice_separate                   		as is_invoice_separate,
		current_term                         		as current_term,
		current_term_period_type               		as current_term_period_type,

		--key_dates
		cancelled_date                       		as cancelled_date,
		contract_acceptance_date              		as contract_acceptance_date,
		contract_effective_date               		as contract_effective_date,
		initial_term                         		as initial_term,
		initial_term_period_type               		as initial_term_period_type,
		term_start_date                       		as term_start_date,
		term_end_date                         		as term_end_date,
		subscription_start_date               		as subscription_start_date,
		subscription_end_date                 		as subscription_end_date,
		service_activation_date               		as service_activation_date,
		opportunity_close_date__qt            		as opportunity_close_date,
		original_created_date                 		as original_created_date,

		--foreign synced info
		opportunity_name__qt                 		as opportunity_name,
		quote_business_type__qt               		as quote_business_type,
		quote_number__qt                     		as quote_number,
		quote_type__qt                       		as quote_type,

		--renewal info
		renewal_setting                      		as renewal_setting,
		renewal_term                         		as renewal_term,
		renewal_term_period_type               		as renewal_term_period_type,

		--metadata
		updated_by_id                         		as updated_by_id,
		updated_date                         		as updated_date,
		created_by_id                         		as created_by_id,
		created_date                         		as created_date


	FROM source

)

SELECT *
FROM renamed