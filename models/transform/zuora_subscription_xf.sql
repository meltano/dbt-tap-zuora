WITH zuora_subscription AS (

    SELECT
        *,
        row_number()
        OVER (
          PARTITION BY
            subscription_name,
            version
          ORDER BY updated_date DESC ) AS sub_row
    FROM {{ ref('zuora_subscription') }}
), 

zuora_subs_fixed AS (

  SELECT * 
  FROM zuora_subscription
  WHERE subscription_status IN ('Active', 'Cancelled')

)

SELECT
  zuora_subs_fixed.*,

  zuora_subs_fixed.subscription_name_slugify AS subscription_slug_for_counting,

  -- Dates
  date_trunc('month', zuora_subs_fixed.subscription_start_date) :: DATE    AS subscription_start_month,
  (zuora_subs_fixed.subscription_end_date - '1 month'::interval)::DATE     AS subscription_end_month,
 
  date_trunc('month', zuora_subs_fixed.contract_effective_date) :: DATE    AS cohort_month,
  date_trunc('quarter', zuora_subs_fixed.contract_effective_date) :: DATE  AS cohort_quarter,
  date_trunc('year', zuora_subs_fixed.contract_effective_date) :: DATE     AS cohort_year

FROM zuora_subs_fixed

WHERE 
  zuora_subs_fixed.sub_row = 1 
