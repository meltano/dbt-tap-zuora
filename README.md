# dbt | tap-zuora

This [dbt](https://github.com/fishtown-analytics/dbt) package contains data models for [tap-zuora](https://github.com/singer-io/tap-zuora).

All the models in this package are based on the [Zuora models](https://gitlab.com/gitlab-data/analytics/tree/master/transform/snowflake-dbt/models/zuora) implemented by [Gitlab's Data Team](https://gitlab.com/gitlab-data/analytics).

You can check the extensive documentation provided as dbt docs for each model. For example, [zuora_mrr_totals](https://gitlab-data.gitlab.io/analytics/dbt/snowflake/#!/model/model.gitlab_snowflake.zuora_mrr_totals) is the final model in amortizing subscriptions to MRR.

Differences between this package and the models in reference: 
1. The models have been converted to work with Postgres (instead of Snowflake)
2. The table names and the column names for the raw tables are the ones generated when `tap-zuora` runs.
